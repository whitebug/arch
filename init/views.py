from django.shortcuts import render


def home(request):
    return render(request, 'init/home.html', locals())
